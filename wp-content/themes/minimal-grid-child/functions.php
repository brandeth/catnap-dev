<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array( 'ionicons','bootstrap','magnific-popup','slick','perfect-scrollbar' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

/*My Custom Scripts*/

// Register the script
wp_register_script( 'some_handle', get_stylesheet_directory_uri() . "/script.js", array('jquery'), null, true );
 
// Localize the script with new data
$translation_array = array(
    'path' => wp_upload_dir()['url']
);
wp_localize_script( 'some_handle', 'object_name', $translation_array );
 
// Enqueued script with localized data.
wp_enqueue_script( 'some_handle' );

// END ENQUEUE PARENT ACTION
