$(document).ready(function() {
	const massageImg = $('.massage-type-images img');
	const massageTitle = $('#massage-title');
	const massageDetails = $('.massage-details');
	const menuPanelWrapper = $('.menu-panel-wrapper');

	/*Massages Section*/
	massageImg.removeAttr('srcset');

	/*$('#spot-massages li a').hover(e => {
		console.log(massageImg.attr('src'))
	}, e => {
		console.log(massageImg.attr('src'))
	})*/

	$('#massages li a').mouseover(function(e) {
		e.preventDefault();
		if(massageImg.attr('src') !== `${object_name.path}/${$(this).attr('data-image')}`) {
			massageImg.attr('src', `${object_name.path}/${$(this).attr('data-image')}`).stop(true,true).hide().fadeIn("fast");
			massageTitle.text($(this).text());
			massageDetails.text($(this).attr('data-detail'));	
		}
	})
	/*End Massages Section*/

	/*Contacts on Sidebar*/
	menuPanelWrapper.append(`
		<div id="nav-panel-contacts">
			<p class="operating-hours"> Operating Hours: <br> Mon - Sun 2PM </p>
			<p> +63 917-678-0989 </p>
			<p> (02) 959-4915 </p>
			<p> 3 Brixton St. Brgy. Kapitolyo, Pasig City </p>
		</div>
	`)
	/*End Contacts on Sidebar*/
});