<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'catnap' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'VQ;[v2w;O%Vzw?6y-y&$z/8wJwY(_P*jP2rq9&@.o&*&[~~f)kIMj{^l2#x)tY*^' );
define( 'SECURE_AUTH_KEY',  'VSX_&>#+Hg^AJGN1J8[cc>E_<Dx% {zga~q-MCSC^F;*NoZ:[,7Tfd=DC0Dm>;Uv' );
define( 'LOGGED_IN_KEY',    '2D@IuX(*3`fcRom25KB8_[9a&4/CKICK3Cn@C`{eftfSrA%;=7&xYQ!LHP?2)#:R' );
define( 'NONCE_KEY',        '8tnv/T=32.)J_Wajtv*,DgW!(7n9gh/1U}I;j$^es+/YC;NNMp%V~l+bNwR8H19U' );
define( 'AUTH_SALT',        'r+&1k:pwx4|mZkfMJ2;T}IgW/+4eB%SvXN1VX){2y23boF?XxJEoRA*|U5&*s<Bx' );
define( 'SECURE_AUTH_SALT', '*{z3/&yq!nJ|4^^q;SC<M-x4,qRp^E8=%G/={5k!gG`4A`#~pgRB`*cvruW$/9CS' );
define( 'LOGGED_IN_SALT',   'yR%=)i+<Wi{9/.@2?hUg^EKLH-e~Pw)dS@oI_fe|C7L#hc} UsYD15qov;mgK_>I' );
define( 'NONCE_SALT',       'k#KB-G_dCg!O <R8AK/^HvkaF[G{bo2(?BL[Ga1H>E!B@a$Ck,MF_Ae>zn50a!~d' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
